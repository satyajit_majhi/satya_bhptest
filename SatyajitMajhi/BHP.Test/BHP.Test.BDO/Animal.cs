﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Contains the model classes for Business layer
/// </summary>
namespace BHP.Test.BDO
{
    /// <summary>
    /// Animal model wiht its different properties
    /// </summary>
    public class Animal
    {
        public string CommonName { get; set; }
        public string ClassName { get; set; }
        public string ScientificName { get; set; }
        public string Habitat { get; set; }
        public Animal(string CommonName, string ClassName, string ScientificName, string Habitat)
        {
            this.CommonName = CommonName;
            this.ClassName = ClassName;
            this.ScientificName = ScientificName;
            this.Habitat = Habitat;
        }        
    }
}
