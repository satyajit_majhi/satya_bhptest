﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHP.Test.BDO
{
   public class AnimalDetail
    {
        public int TotalNoOfResults { get; set; }
        public Collection<Animal> AnimalCollection { get; set; }
    }
}
