﻿var ngApp = angular.module('BHPTestNgApp');
ngApp.controller('PrimeNumberController', function ($scope, $http) {
    $scope.GetNextPrimeNumber = function () {
        $http({
            method: "GET",
            url: "http://localhost:53535/api/prime/GetNextPrimeNumber",
        }).then(function Success(response) {
            $scope.primeNumber = response.data;
        }, function Error(response) {
            $scope.primeNumber = response.status + ':' + response.statusText;
        });
    }
});