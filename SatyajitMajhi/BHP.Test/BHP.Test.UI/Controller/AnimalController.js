﻿var ngApp = angular.module('BHPTestNgApp', ["kendo.directives"]);
ngApp.controller('AnimalController', function ($scope, $http) {
    $scope.className = "";
    $scope.commonName = "";
    $scope.scientificName = "";
    $scope.habitat = "";
    $scope.FindBySpecificationRequest = function () {
        $scope.gridAnimals = {
            dataSource: {
                serverPaging: true,
                pageSize: 5,
                type: "json",
                transport: {
                    read: {
                        url: "http://localhost:53535/api/animal/FindBySpecificationRequest?commonNameStartsWith=" + $scope.commonName + "&classNameStartsWith=" + $scope.className + "&scientificNameStartsWith=" + $scope.scientificName + "&habitat=" + $scope.habitat,
                        type: "GET",
                        dataType: "json",
                    },
                },
                schema:
                {
                    model: {
                        fields: {
                            CommonName: { type: "string" },
                            ClassName: { type: "string" },
                            ScientificName: { type: "string" },
                            Habitat: { type: "string" }
                        }
                    },
                    total: "TotalNoOfResults",
                    data: function (result) {
                        return result.AnimalCollection
                    },
                },
            },
            pageSize: 5,
            pageable: {
                refresh: true,
                pageSizes: [5, 10],
            },
            columns: [{
                field: "CommonName",
                title: "Common Name",
            }, {
                field: "ClassName",
                title: "Class Name",
            },
            {
                field: "ScientificName",
                title: "Scientific Name",
            },
            {
                field: "Habitat",
                title: "Habitat",
            }]
        };
    }
});