﻿var ngApp = angular.module('BHPTestNgApp');
ngApp.controller('DurationController', function ($scope, $http) {
    $scope.GetDurationInWordFormat = function (duration) {
        $http({
            method: "GET",
            url: "http://localhost:53535/api/duration/GetDuration?duration=" + duration,
        }).then(function Success(response) {
            $scope.durationInwords = response.data;
        }, function Error(response) {
            $scope.durationInwords = response.status + ':' + response.statusText;
        });
    }
});