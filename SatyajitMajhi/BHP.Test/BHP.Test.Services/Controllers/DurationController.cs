﻿using BHP.Test.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHP.Test.Services.Controllers
{
    public class DurationController : ApiController
    {
        /// <summary>
        /// Validate the time duration and return the duration in written form
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetDurationInWordFormat(string duration)
        {
            DurationBL durationBL = new DurationBL();
            if (durationBL.IsValidateDuration(duration))
                return Ok(durationBL.GetDurationInWordFormat(duration));
            else
                return BadRequest();
        }
    }
}
