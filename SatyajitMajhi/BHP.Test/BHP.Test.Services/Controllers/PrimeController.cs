﻿using BHP.Test.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHP.Test.Services.Controllers
{
    public class PrimeController : ApiController
    {
        /// <summary>
        /// Get next prime number in the sequence regardless of which user makes the request
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetNextPrimeNumber()
        {

            PrimeBL primeBL = new PrimeBL();
            return Ok(primeBL.GetNextPrimeNumber());
        }
    }
}
