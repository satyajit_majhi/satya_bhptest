﻿using BHP.Test.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHP.Test.Services.Controllers
{
    public class AnimalController : ApiController
    {

        /// <summary>
        /// Search animal details based on below criterea
        /// If CommonNameStartsWith is specified then only animals where CommonName starts with this text should be returned.
        /// If ClassNameStartsWith is specified then only animals where ClassName starts with this text should be returned.
        /// If ScientificNameStartsWith is specified then only animals where ScientificNameStartsWith starts with this text should be returned.
        /// If Habitat is specified then only animals where Habitat matches this text should be returned.
        /// </summary>
        /// <param name="commonNameStartsWith"></param>
        /// <param name="classNameStartsWith"></param>
        /// <param name="scientificNameStartsWith"></param>
        /// <param name="habitat"></param>
        /// <returns></returns>        
        [HttpGet]
        public IHttpActionResult FindBySpecificationRequest(string commonNameStartsWith, string classNameStartsWith, string scientificNameStartsWith, string habitat ,int take,int skip,int page,int pageSize)
        {
            AnimalBL animalBL = new AnimalBL(); ;
            var animalResultCollection = animalBL.SearchAnimals(commonNameStartsWith, classNameStartsWith, scientificNameStartsWith, habitat, skip, take);
            return Ok(animalResultCollection);
        }
    }
}
