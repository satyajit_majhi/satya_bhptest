﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHP.Test.BL
{
    public class PrimeBL
    {
        /// <summary>
        /// Get next prime number in the sequence regardless of which user makes the request
        /// </summary>
        /// <returns></returns>
        public Int64 GetNextPrimeNumber()
        {
            object primeNumberLock = new object();

            //Apply thread safe  
            lock (primeNumberLock)
            {
                Int64 number = SingletonPrimeNumber.GetInstance.primeNumber;

                while (true)
                {
                    bool isPrime = true;
                    number = number + 1;
                    //Start at 2 and increment by 1 till the square of it is less than and equal to the number
                    for (int i = 2; i * i <= number; i++)
                    {
                        //If the number is divisible by any other numbers
                        //then its not a prime number
                        if (number % i == 0)
                        {
                            isPrime = false;
                            break;
                        }
                    }
                    if (isPrime && number != 1)
                    {
                        SingletonPrimeNumber.GetInstance.primeNumber = number;
                        return number;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Singleton class for prime number
    /// </summary>
    public sealed class SingletonPrimeNumber
    {
        public Int64 primeNumber;
        private static SingletonPrimeNumber singletonInstance = null;
        private static readonly object singletonInstanceLock = new object();
        private SingletonPrimeNumber() { }

        /// <summary>
        /// Get singleton instance of the prime number
        /// </summary>
        public static SingletonPrimeNumber GetInstance
        {
            get
            {
                lock (singletonInstanceLock)
                {
                    if (singletonInstance == null)
                    {
                        singletonInstance = new SingletonPrimeNumber();
                    }
                    return singletonInstance;
                }
            }
        }
    }
}
