﻿using BHP.Test.BDO;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace BHP.Test.BL
{
    /// <summary>
    /// Contains business logic related animal 
    /// </summary>
    public class AnimalBL
    {
        Collection<Animal> sampleAnimals = new Collection<Animal>();
        public AnimalBL()
        {
            //set sample animals
            sampleAnimals = GetSampleAnimalCollections();
        }

        /// <summary>
        /// Search animal details based on below criterea
        /// If CommonNameStartsWith is specified then only animals where CommonName starts with this text should be returned.
        /// If ClassNameStartsWith is specified then only animals where ClassName starts with this text should be returned.
        /// If ScientificNameStartsWith is specified then only animals where ScientificNameStartsWith starts with this text should be returned.
        /// If Habitat is specified then only animals where Habitat matches this text should be returned.
        /// </summary>
        /// <param name="commonName"></param>
        /// <param name="className"></param>
        /// <param name="scientificName"></param>
        /// <param name="habitat"></param>
        /// <returns></returns>
        public AnimalDetail SearchAnimals(string commonName, string className, string scientificName, string habitat, int skip, int take)
        {
            var animalDetails = new AnimalDetail();
            Collection<Animal> searchedAnimals = new Collection<Animal>((from animals in sampleAnimals
                                                                         where (!string.IsNullOrEmpty(className) ? animals.ClassName.StartsWith(className, StringComparison.InvariantCultureIgnoreCase) : true)
                                                                         && (!string.IsNullOrEmpty(commonName) ? animals.CommonName.StartsWith(commonName, StringComparison.InvariantCultureIgnoreCase) : true)
                                                                         && (!string.IsNullOrEmpty(scientificName) ? animals.ScientificName.StartsWith(scientificName, StringComparison.InvariantCultureIgnoreCase) : true)
                                                                         && (!string.IsNullOrEmpty(habitat) ? animals.Habitat.ToLower().Contains(habitat.ToLower()) : true)
                                                                      select animals).ToList());
            if (searchedAnimals !=null)
            {
                animalDetails.TotalNoOfResults = searchedAnimals.Count();
                animalDetails.AnimalCollection = new Collection<Animal>(searchedAnimals.Skip(skip).Take(take).ToList());
            }           
            return animalDetails;
        }

        /// <summary>
        /// Get sample animals
        /// </summary>
        /// <returns></returns>
        public Collection<Animal> GetSampleAnimalCollections()
        {
            Collection<Animal> sampleAnimals = new Collection<Animal>();
            sampleAnimals.Add(new Animal("Horse", "Mammalia", "Equus Caballus", "Small forests and grassland"));
            sampleAnimals.Add(new Animal("Gibbon", "Mammalia", "Hylobatidae", "Dense forest and jungle"));
            sampleAnimals.Add(new Animal("Fox", "Mammalia", "Vulpes vulpes", "Forest"));
            sampleAnimals.Add(new Animal("Cheetah", "Mammalia", "Acinonyx jubatus", "Forest"));
            sampleAnimals.Add(new Animal("Leopard", "Mammalia", "Panthera pardus", "Rainforest, grassland and mountainous regions"));
            sampleAnimals.Add(new Animal("Camel", "Mammalia", " Camelus dromedarius", "Desert"));
            sampleAnimals.Add(new Animal("Penguin", "Aves", "Aptenodytes Forsteri", "Cold Sea,Iceland"));
            sampleAnimals.Add(new Animal("Buffalo", "Mammalia", "Syncerus Caffer", "Woodland and grass pastures"));
            sampleAnimals.Add(new Animal("Ostrich", "Aves", "Struthio Camelus", "Desert and savanna areas"));
            sampleAnimals.Add(new Animal("Bat", "Mammalia", "Chiroptera", "Woodland and caves"));
            sampleAnimals.Add(new Animal("Lion", "Mammalia", "Panthera leo", "open woodland, scrub, grassland"));

            return sampleAnimals;
        }

    }
}
