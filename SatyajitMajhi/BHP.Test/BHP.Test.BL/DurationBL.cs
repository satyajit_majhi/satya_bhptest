﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHP.Test.BL
{
    public class DurationBL
    {
        /// <summary>
        /// Check if the duration is valid
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="duartionArray"></param>
        /// <returns></returns>
        public bool IsValidateDuration(string duration)
        {
            bool isValidDuration = false;
            string[] durationArray = null;
            if (!string.IsNullOrEmpty(duration) && duration.Contains(':'))
            {
                durationArray = duration.Split(':');

                //Check duration contains only one colon ':' then validate
                if (durationArray.Length > 0 && durationArray.Length == 2 && durationArray[0].Length <= 2 && durationArray[1].Length <= 2)
                {
                    bool isHourConversionSuccess = int.TryParse(durationArray[0], out int hours);
                    bool isMinuteConversionSuccess = int.TryParse(durationArray[1], out int minutes);
                    if (isHourConversionSuccess && isMinuteConversionSuccess
                        && hours >= 0 && hours <= 12 && minutes >= 0 && minutes <= 59 && (hours != 0 || minutes != 0))
                    {
                        if (hours < 12 || (hours == 12 && minutes == 0))
                            isValidDuration = true;
                    }
                }
            }
            return isValidDuration;
        }

        /// <summary>
        /// Get duration details in word format
        /// </summary>
        /// <param name="duartionArray"></param>
        /// <returns></returns>
        public string GetDurationInWordFormat(string duration)
        {
            string[] durationArray = duration.Split(':');
            string durationInWords = string.Empty;
            string hourInWords = GetNumberInWords(Convert.ToInt32(durationArray[0]));
            string minuteInWords = GetNumberInWords(Convert.ToInt32(durationArray[1]));

            if (!string.IsNullOrEmpty(hourInWords))
            {
                if (hourInWords.Equals("one", StringComparison.InvariantCultureIgnoreCase))
                    durationInWords = hourInWords + " hour";
                else
                    durationInWords = hourInWords + " hours";
            }
            if (!string.IsNullOrEmpty(minuteInWords))
            {
                if (!string.IsNullOrEmpty(durationInWords))
                    durationInWords += " and ";
                if (minuteInWords.Equals("one", StringComparison.InvariantCultureIgnoreCase))
                    durationInWords = durationInWords + minuteInWords + " minute";
                else
                    durationInWords = durationInWords + minuteInWords + " minutes";
            }
            return durationInWords;
        }

        /// <summary>
        /// Get numbers in words
        /// Valid till number 59
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string GetNumberInWords(int number)
        {
            string numberInWords = "";
            if (number < 60)
            {
                switch (number)
                {
                    case 1:
                        numberInWords = "one";
                        break;
                    case 2:
                        numberInWords = "two";
                        break;
                    case 3:
                        numberInWords = "three";
                        break;
                    case 4:
                        numberInWords = "four";
                        break;
                    case 5:
                        numberInWords = "five";
                        break;
                    case 6:
                        numberInWords = "six";
                        break;
                    case 7:
                        numberInWords = "seven";
                        break;
                    case 8:
                        numberInWords = "eight";
                        break;
                    case 9:
                        numberInWords = "nine";
                        break;
                    case 10:
                        numberInWords = "ten";
                        break;
                    case 11:
                        numberInWords = "eleven";
                        break;
                    case 12:
                        numberInWords = "twelve";
                        break;
                    case 13:
                        numberInWords = "thirteen";
                        break;
                    case 14:
                        numberInWords = "fourteen";
                        break;
                    case 15:
                        numberInWords = "fifteen";
                        break;
                    case 16:
                        numberInWords = "sixteen";
                        break;
                    case 17:
                        numberInWords = "seventeen";
                        break;
                    case 18:
                        numberInWords = "eighteen";
                        break;
                    case 19:
                        numberInWords = "nineteen";
                        break;
                    case 20:
                        numberInWords = "twenty";
                        break;
                    case 30:
                        numberInWords = "thirty";
                        break;
                    case 40:
                        numberInWords = "fourty";
                        break;
                    case 50:
                        numberInWords = "fifty";
                        break;
                    default:
                        //If number is like 33, 45, 29 etc..
                        //Get the tens first and then the last digit (Ex : 24 should be 20 and 4 then convert it to words)
                        if (number > 0)
                        {
                            numberInWords = GetNumberInWords((int)(Math.Floor((double)number / 10.0)) * 10) + " " + GetNumberInWords((int)((double)number % 10.0));
                        }
                        break;
                }
            }
            return numberInWords;
        }
    }
}
